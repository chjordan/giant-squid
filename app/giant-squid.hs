-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# OPTIONS_GHC -Wno-partial-fields #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE NumDecimals       #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main
  ( main
  )
where

import           Control.Concurrent                       ( threadDelay )
import           Control.Exception.Safe                   ( SomeException(..) )
import           Control.Monad
import qualified Data.ByteString.Lazy.Char8    as BL
import qualified Data.HashMap.Lazy             as HM
import           Data.Maybe                               ( isJust )
import           Data.Text                                ( Text )
import qualified Data.Text                     as T
import           Data.Time.Clock                          ( getCurrentTime )
import           Data.Time.LocalTime                      ( utcToLocalZonedTime )
import           Data.Version                             ( showVersion )
import           Network.HTTP.Client                      ( CookieJar )
import           Options.Applicative
import           Options.Applicative.Help                 ( align
                                                          , bold
                                                          , putDoc
                                                          , text
                                                          , vsep
                                                          )
import           System.Environment                       ( lookupEnv )

import           GiantSquid.ASVO
import           GiantSquid.Types
import           GiantSquid.Utils
import qualified Paths_giant_squid             as GS


data Options = OtherOptions
  { version  :: !Bool
  , list     :: !Bool
  , json     :: !Bool
  , noColour :: !Bool
  , debug    :: !Bool
  }
  | Download
  { jobIdsOrObsids :: [Text]
  , keepZip        :: !Bool
  , hash           :: !Bool
  , verbose        :: !Bool
  , debug          :: !Bool
  , dryRun         :: !Bool
  }
  | SubmitVis
    { obsids  :: [Text]
    , verbose :: !Bool
    , wait    :: !Bool
    , dryRun  :: !Bool
    }
  | SubmitConv
  { obsids           :: [Text]
  , conversionParams :: Maybe Text
  , verbose          :: !Bool
  , wait             :: !Bool
  , dryRun           :: !Bool
  } deriving (Show)

otherOptionsParser :: Parser Options
otherOptionsParser =
  OtherOptions
    <$> switch (short 'V' <> long "version" <> help "Display the version and exit")
    <*> switch (short 'l' <> long "list" <> help "List the status of your ASVO jobs")
    <*> switch (short 'j' <> long "json" <> help "Output your ASVO queue as JSON")
    <*> (switch (short 'n' <> long "no-colour" <> help "Do not display colours when listing jobs")
        <|> switch (long "no-color" <> help "Alias for 'no-colour'")
        )
    <*> switch (short 'd' <> long "debug" <> help "Debug the list operation")

optionsParser :: Parser Options
optionsParser = otherOptionsParser <|> hsubparser
  (  command
      "submit-vis"
      (info
        (   SubmitVis
        <$> some
              (strArgument
                (  metavar "OBSID..."
                <> help
                     "Submit obsids to ASVO for visibility download. Text files containing obsids are also accepted and unpacked. Multiple obsids should be space-separated (e.g. \"1065880128 1065880248\")"
                )
              )
        <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
        <*> switch
              (short 'w' <> long "wait" <> help
                "Do not exit giant-squid until the obsid is ready for download"
              )
        <*> switch
              (short 'n' <> long "dry-run" <> help
                "Do not actually submit jobs; just simulate the operation"
              )
        )
        (  headerDoc
            (  Just
            $  bold "giant-squid submit-vis"
            <> ": Submit obsids to the ASVO for visibility download"
            )
        <> progDesc "e.g. giant-squid submit-vis 1065880128 1065880248"
        )
      )
  <> command
       "submit-conv"
       (info
         (   SubmitConv
         <$> some
               (strArgument
                 (  metavar "OBSID..."
                 <> help
                      "Submit obsids to ASVO for conversion download. Text files containing obsids are also accepted and unpacked. Multiple obsids should be space-separated (e.g. \"1065880128 1065880248\"). This option should be used in conjunction with the option \"conversion-parameters\""
                 )
               )
         <*> optional
               (strOption
                 (short 'p' <> long "conversion-parameters" <> helpDoc
                   (Just $ vsep
                     [ "cotter parameters to be used for conversion jobs. If any of the "
                     , "default parameters are not overwritten, then they remain."
                     , "Default: " <> align
                       (vsep
                         (   text
                         .   T.unpack
                         <$> T.lines
                               (   T.intercalate ",\n"
                               $   (\(k, v) -> k <> "=" <> v)
                               <$> HM.toList defaultConversionParameters
                               )
                         )
                       )
                     ]
                   )
                 )
               )
         <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
         <*> switch
               (short 'w' <> long "wait" <> help
                 "Do not exit giant-squid until the obsid is ready for download"
               )
         <*> switch
               (short 'n' <> long "dry-run" <> help
                 "Do not actually submit jobs; just simulate the operation"
               )
         )
         (  headerDoc
             (  Just
             $  bold "giant-squid submit-conv"
             <> ": Submit obsids to the ASVO for cotter conversion"
             )
         <> progDesc
              "e.g. giant-squid submit-conv 1065880128 1065880248 -p timeres=0.5,freqres=10"
         )
       )
  <> command
       "download"
       (info
         (   Download
         <$> some
               (strArgument
                 (  metavar "JOBID/OBSID..."
                 <> help
                      "Specify ASVO job IDs or obsids to be downloaded (e.g. 92861 or 1065880128). giant-squid differentiates between job IDs and obsids by inspecting the length of the number; 10-digit numbers are assumed to be obsids. Text files containing job IDs or obsids are also accepted and unpacked. Multiple job IDs, obsids or files should be space-separated (e.g. \"92861 1065880128 92862\")"
                 )
               )
         <*> switch
               (short 'k' <> long "keep-zip" <> help
                 "Do not unpack any downloaded zips, just download them"
               )
         <*> switch (long "hash" <> help "Verify the integrity of the download")
         <*> switch (short 'v' <> long "verbose" <> help "Display more messages")
         <*> switch
               (short 'd' <> long "debug" <> help "Display extra information for debugging")
         <*> switch
               (short 'n' <> long "dry-run" <> help
                 "Do not actually download jobs; just simulate the operation"
               )
         )
         (  headerDoc
             (Just $ bold "giant-squid download" <> ": Download ASVO job IDs and/or obsids")
         <> progDesc "e.g. giant-squid download 92861 92862 1065880128 1065880248"
         )
       )
  )

getCookies debug = do
  when debug $ putStr "Connecting to ASVO... "
  cookies <- connectToAsvo
  when debug $ putStrLn "done."
  return cookies

-- | @threadDelay@ in seconds
sleep :: Int -> IO ()
sleep = threadDelay . (* 1e6)

waitUntilReady :: CookieJar -> [AsvoJobId] -> IO ()
waitUntilReady cookies jids = do
  queue <- getAsvoQueue cookies

  forM_ jids $ \jid ->
    let sjid = show jid
    in
      case HM.toList $ HM.filter (\AsvoJob { jobId } -> jid == jobId) queue of
        [] ->
          errorWithoutStackTrace $ "JobId " ++ sjid ++ " isn't in the queue! This shouldn't happen."
        [(_, meta)] -> case jobState meta of
          Ready     -> return ()
          Error e   -> errorWithoutStackTrace $ T.unpack e
          Expired   -> errorWithoutStackTrace $ "JobId " ++ sjid ++ " has expired."
          Cancelled -> errorWithoutStackTrace $ "JobId " ++ sjid ++ " has been cancelled."
          _         -> sleep 60 >> waitUntilReady cookies jids
        _ ->
          errorWithoutStackTrace
            $  "Job ID "
            ++ sjid
            ++ " is associated with multiple jobs! This is a weird one..."

  time <- getCurrentTime >>= utcToLocalZonedTime
  putStrLn $ "Obsid(s) ready at " ++ show time

main :: IO ()
main = do
  let
    oa = info
      (optionsParser <**> helper)
      (headerDoc
        (Just $ vsep
          [ bold "giant-squid" <> " - An alternative MWA ASVO client."
          , "    Version: " <> text (showVersion GS.version)
          , "    Source: https://gitlab.com/chjordan/giant-squid"
          , "    MWA ASVO: https://asvo.mwatelescope.org"
          ]
        )
      )
    oaPrefs = prefs (showHelpOnEmpty <> disambiguate)
    printDefaultHelpText =
      handleParseResult . Failure $ parserFailure oaPrefs oa ShowHelpText mempty
  opts <- customExecParser oaPrefs oa

  case opts of
    OtherOptions {..} -> if
      | version -> putStrLn (showVersion GS.version)
      | json -> getCookies debug >>= getJsonAsvoQueue >>= BL.putStrLn
      | list -> getCookies debug >>= \cookies -> if debug
        then getRawAsvoQueue cookies >>= print
        else do
          -- To comply with https://no-color.org/, check if NO_COLOR or
          -- NO_COLOUR are defined.
          noColourEnvDefined <- lookupEnv "NO_COLOUR"
          noColorEnvDefined  <- lookupEnv "NO_COLOR"
          printAsvoQueue cookies
            $  not
            $  noColour -- Also check the command-line option.
            || isJust noColourEnvDefined
            || isJust noColorEnvDefined
      | otherwise -> printDefaultHelpText

    Download {..} -> do
      cookies <- getCookies verbose
      mis     <- mapM (parseTextToFileOrInt verbose) jobIdsOrObsids
      -- Require all values in `jobIdsOrObsids` to successfully parse as Ints.
      case sequence (concat mis) of
        Left (SomeException e) -> errorWithoutStackTrace $ show e
        Right is ->
          -- Try to parse the Ints as Obsids; any that fail are assumed to be
          -- ASVO job IDs.
          let eitherOJ =
                  (\i -> case intToObsid i of
                      Left  _ -> Right $ AsvoJobId i
                      Right o -> Left o
                    )
                    <$> is
          in  unless dryRun
                $   forM_ eitherOJ
                $   downloadAsvoJob verbose cookies hash keepZip
                >=> (\case
                      Left  e -> print e
                      Right _ -> return ()
                    )

    SubmitVis {..} -> do
      mos <- mapM (parseTextToFileOrObsid verbose) obsids
      case sequence (concat mos) of
        Left  (SomeException e) -> errorWithoutStackTrace $ show e
        Right os                -> do
          cookies <- getCookies verbose
          if dryRun
            then putStrLn
              ("Would have submitted " <> show (length os) <> " obsids for visibility download.")
            else
              forM
                  os
                  (\o -> submitAsvoVisibilityJob cookies o >>= \jid -> do
                    putStrLn $ "Submitted " <> show o <> " as ASVO job ID " <> show jid
                    return jid
                  )
                >>= \jids ->
                      when
                          (length os > 1)
                          (putStrLn
                            ("Submitted " <> show (length os) <> " obsids for visibility download.")
                          )
                        >> when wait (waitUntilReady cookies jids)

    SubmitConv {..} -> do
      let conversionParameters = encodeConversionParameters $ case conversionParams of
            Nothing -> defaultConversionParameters
            Just co -> HM.union (textParametersToHashMap co) defaultConversionParameters
      when verbose
        $  putStrLn "Conversion parameters:"
        >> putDoc (prettyPrintConversionParams conversionParameters)
        >> putStrLn "\n"

      mos <- mapM (parseTextToFileOrObsid verbose) obsids
      case sequence (concat mos) of
        Left  (SomeException e) -> errorWithoutStackTrace $ show e
        Right os                -> do
          cookies <- getCookies verbose
          if dryRun
            then putStrLn ("Would have submitted " <> show (length os) <> " obsids for conversion.")
            else
              forM
                  os
                  (\o -> submitAsvoConversionJob cookies conversionParameters o >>= \jid -> do
                    putStrLn $ "Submitted " <> show o <> " as ASVO job ID " <> show jid
                    return jid
                  )
                >>= \jids ->
                      when
                          (length os > 1)
                          (putStrLn ("Submitted " <> show (length os) <> " obsids for conversion."))
                        >> when wait (waitUntilReady cookies jids)
