-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE NumDecimals       #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module GiantSquid.Utils
  ( tshow
  , textToInt
  , intToObsid
  , textToObsid
  , fileToInts
  , fileToObsids
  , parseTextToFileOrInt
  , parseTextToFileOrObsid
  , extractZip
  )
where

import           Conduit
import qualified Data.ByteString.Char8         as B8
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO
import           Data.Time.LocalTime
import           System.FilePath.Posix                    ( takeDirectory )
import           System.IO

import           Codec.Archive.Zip.Conduit.UnZip

import           GiantSquid.Imports
import           GiantSquid.Types


tshow :: Show a => a -> Text
tshow = T.pack . show

textToInt :: MonadThrow m => Text -> m Int
textToInt s = case decimal s of
  Left  _                  -> notAnInt s
  Right (obsid, remainder) -> if T.null remainder then return obsid else notAnInt s
  where notAnInt t = throw . NotIntException $ show t

intToObsid :: MonadThrow m => Int -> m Obsid
intToObsid i =
  if i >= 1e9 && i < 1e10 then return $ Obsid i else throw . NotObsidException $ show i

textToObsid :: MonadThrow m => Text -> m Obsid
textToObsid = textToInt >=> intToObsid

{-| Read the supplied file, and return a list of 'Int'. Each element is an
instance of 'MonadThrow', which allows the return type to be
polymorphic. e.g. @['Maybe' 'Obsid']@ or @['Either' 'SomeException' 'Obsid']@.
-}
fileToInts :: (MonadIO m, MonadThrow n) => FilePath -> m [n Int]
fileToInts = fmap f . liftIO . TIO.readFile where f t = textToInt <$> T.words t

-- | The same as @fileToInts@, but for 'Obsid'.
fileToObsids :: (MonadIO m, MonadThrow n) => FilePath -> m [n Obsid]
fileToObsids = fmap f . liftIO . TIO.readFile where f t = textToObsid <$> T.words t

{-| Take a 'Text' value, and if it is the path to a file, then parse its contents
  into a list of 'Int' values. If the 'Text' value is not the path to a file, then
  parse it into an 'Int' value.
-}
parseTextToFileOrInt
  :: (MonadIO m, MonadThrow n)
  => Bool -- ^ If True, print a message when opening a file.
  -> Text
  -> m [n Int]
parseTextToFileOrInt v t = do
  let filename = T.unpack t
  fileExists <- liftIO $ doesFileExist filename
  if fileExists
    then do
      liftIO $ when v $ putStrLn $ "Opening " <> filename <> " ..."
      fileToInts filename
    else return [textToInt t]

-- | The same as @parseTextToFileOrInt@, but for 'Obsid'.
parseTextToFileOrObsid :: (MonadIO m, MonadThrow n) => Bool -> Text -> m [n Obsid]
parseTextToFileOrObsid v t = do
  let filename = T.unpack t
  fileExists <- liftIO $ doesFileExist filename
  if fileExists
    then do
      liftIO $ when v $ putStrLn $ "Opening " <> filename <> " ..."
      fileToObsids filename
    else return [textToObsid t]

-- | Derived from https://github.com/dylex/zip-stream/blob/master/cmd/unzip.hs
extractZip :: MonadResource m => ConduitM (Either ZipEntry ByteString) Void m ()
extractZip = awaitForever start
 where
  start (Left ZipEntry {..}) = do
    liftIO $ createDirectoryIfMissing True (takeDirectory name)
    if either T.last B8.last zipEntryName == '/'
      then when ((0 /=) `any` zipEntrySize) $ fail $ name ++ ": non-empty directory"
      else do  -- C.bracketP
        liftIO (doesFileExist name) >>= flip when (liftIO $ removeFile name)
        h <- liftIO $ openFile name WriteMode
        mapM_ (liftIO . hSetFileSize h . toInteger) zipEntrySize
        write .| sinkHandle h
        liftIO $ hClose h
    liftIO $ setModificationTime name $ localTimeToUTC utc zipEntryTime  -- FIXME: timezone
   where
    name =
      either (T.unpack . T.dropWhile ('/' ==)) (B8.unpack . B8.dropWhile ('/' ==)) zipEntryName
  start (Right _) = fail "Unexpected leading or directory data contents"
  write :: MonadResource m => ConduitT (Either a o) o m ()
  write = await >>= maybe (return ()) block
  block (Right b) = yield b >> write
  block a         = leftover a
