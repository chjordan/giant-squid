-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE NumDecimals                #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module GiantSquid.Types
  ( Obsid(..)
  , AsvoQueue
  , AsvoJob(..)
  , AsvoJobId(..)
  , AsvoJobType(..)
  , AsvoJobState(..)
  , AsvoJobParameters
  , AsvoFilesArray(..)
  , AsvoSubmitJobResponseBody(..)
  , NumQueued(..)
  , NumProcessing(..)
  , NumReady(..)
  , NumError(..)
  , AsvoQueueException(..)
  , AsvoResponseException(..)
  , NotIntException(..)
  , NotObsidException(..)
  )
where

import           Data.Aeson                               ( (.:)
                                                          , (.:?)
                                                          )
import qualified Data.Aeson                    as A
import           Data.Aeson.Types                         ( Parser )
import           Data.Hashable                            ( Hashable )
import qualified Data.Text                     as T
import qualified Data.Vector                   as V
import           GHC.Generics                             ( Generic )

import           GiantSquid.Imports


newtype Obsid = Obsid { unObsid :: Int }
  deriving stock (Eq, Ord, Read)
  deriving newtype (Hashable)
instance Show Obsid where
  show = show . unObsid
instance A.ToJSON Obsid where
  toJSON = A.toJSON . unObsid

newtype AsvoJobId = AsvoJobId { unAsvoJobId :: Int }
  deriving stock (Eq, Ord)
  deriving newtype (Hashable)
instance Show AsvoJobId where
  show = show . unAsvoJobId
instance A.ToJSON AsvoJobId where
  toJSON = A.toJSON . unAsvoJobId

type AsvoQueue = HashMap AsvoJobId AsvoJob
newtype NumQueued = NumQueued { unNumQueued :: Int }
newtype NumProcessing = NumProcessing { unNumProcessing :: Int }
newtype NumReady = NumReady { unNumReady :: Int }
newtype NumError = NumError { unNumError :: Int }
type AsvoJobParameters = HashMap ByteString ByteString

data AsvoJobType = Conversion
                 | DownloadVisibilities
                 | DownloadMetadata
                 | DownloadVoltage
                 | CancelJob
  deriving (Eq, Generic)
instance A.ToJSON AsvoJobType

instance Show AsvoJobType where
  show Conversion           = "Conversion"
  show DownloadVisibilities = "Download Visibilities"
  show DownloadMetadata     = "Download Metadata"
  show DownloadVoltage      = "Download Voltage"
  show CancelJob            = "Cancel Job"

data AsvoJobState = Queued
                  | Processing
                  | Ready
                  | Error Text
                  | Expired
                  | Cancelled
  deriving (Show, Eq)
instance A.ToJSON AsvoJobState where
  toJSON jobState = case jobState of
    Error t -> A.String $ "Error: " <> t
    s       -> A.String $ T.pack $ show s

data AsvoFilesArray = AsvoFilesArray
  { fileName :: !FilePath
  , fileSize :: !Integer
  , fileHash :: !String
  } deriving (Show, Eq, Generic)
instance A.ToJSON AsvoFilesArray

data AsvoJob = AsvoJob
  { obsid    :: {-# UNPACK #-} !Obsid
  , jobId    :: {-# UNPACK #-} !AsvoJobId
  , jobType  :: !AsvoJobType
  , jobState :: !AsvoJobState
  , files    :: Maybe [AsvoFilesArray]
  } deriving (Eq, Generic)

instance Show AsvoJob where
  show AsvoJob {..} = concat
    [ show obsid
    , ", jobID: "
    , show jobId
    , " jobType: "
    , show jobType
    , " jobState: "
    , show jobState
    , " productArray: "
    , show files
    ]

newtype NotIntException = NotIntException String
instance Show NotIntException where
  show (NotIntException s) = s ++ " cannot be parsed as an int!"
instance Exception NotIntException

newtype NotObsidException = NotObsidException String
instance Show NotObsidException where
  show (NotObsidException s) = s ++ " cannot be an Obsid, as it does not have 10 digits!"
instance Exception NotObsidException

data AsvoQueueException = JobIdNotInQueue AsvoJobId
                        | ObsidNotInQueue Obsid
                        | JobIdNotReady AsvoJobId
                        | ObsidNotReady Obsid
                        | MultipleFilesObsidException Obsid
                        | MultipleFilesJobIdException AsvoJobId
                        | EmptyProductArrayException Obsid AsvoJobId
                        | NonUnityProductArrayException Obsid AsvoJobId
instance Show AsvoQueueException where
  show (JobIdNotInQueue j) = "Job ID " ++ show j ++ " is not in your ASVO queue!"
  show (ObsidNotInQueue o) = "Obsid " ++ show o ++ " is not in your ASVO queue!"
  show (JobIdNotReady   j) = "Job ID " ++ show j ++ " is not ready!"
  show (ObsidNotReady   o) = "Obsid " ++ show o ++ " is not ready!"
  show (MultipleFilesObsidException o) =
    "Obsid " ++ show o ++ " is associated with multiple jobs! This is a weird one..."
  show (MultipleFilesJobIdException j) =
    "Job ID " ++ show j ++ " is associated with multiple jobs! This is a weird one..."
  show (EmptyProductArrayException o j) =
    "The productArray an ASVO associated with job ID "
      ++ show j
      ++ " (obsid: "
      ++ show o
      ++ ") is empty! This is a weird one..."
  show (NonUnityProductArrayException o j) =
    "Unhandled behaviour: Job ID "
      ++ show j
      ++ " (obsid: "
      ++ show o
      ++ ") has zero or multiple files associated on ASVO!"
instance Exception AsvoQueueException

data AsvoResponseException = AsvoResponseException [(ByteString, ByteString)] Request (Response ByteString)
instance Show AsvoResponseException where
  show (AsvoResponseException jobParams request response) =
    "Invalid response received.\nJob parameters:"
      <> show jobParams
      <> "\n\nRequest:\n"
      <> show request
      <> "\nResponse:\n"
      <> show response
instance Exception AsvoResponseException

parseArrayIndex :: (A.FromJSON a) => Vector A.Value -> Int -> Parser a
parseArrayIndex vec i = case vec V.!? i of
  Nothing  -> fail $ "Index " ++ show i ++ " not present in JSON array"
  Just val -> A.parseJSON val

instance A.FromJSON AsvoFilesArray where
  parseJSON = A.withArray "AsvoJob"
    $ \o -> AsvoFilesArray <$> parseArrayIndex o 0 <*> parseArrayIndex o 1 <*> parseArrayIndex o 2

instance A.FromJSON AsvoJob where
  parseJSON = A.withEmbeddedJSON "AsvoJob" $ \o' -> do
    o                         <- A.parseJSON o'  -- Need this, because ASVO JSON arrays are wrapped in double quotes.
    (_ :: Text)               <- o .: "action"
    (_ :: Text)               <- o .: "table"
    row                       <- o .: "row"
    (job_type :: Int )        <- row .: "job_type"
    (job_state :: Int)        <- row .: "job_state"
    (_ :: Int        )        <- row .: "user_id"
    job_params                <- row .: "job_params"
    (_ :: Maybe Text        ) <- job_params .:? "download_type"
    (obs_id :: Text         ) <- job_params .: "obs_id"
    (_ :: Maybe Int         ) <- row .:? "error_code"
    (errorText :: Maybe Text) <- row .:? "error_text"
    (_ :: Text              ) <- row .: "created"
    (_ :: Text              ) <- row .: "modified"
    (_ :: Maybe Text        ) <- row .:? "expiry_date"
    files                     <- row .:? "product" >>= \case
      Just productArray -> productArray .: "files"
      Nothing           -> return Nothing
    (_ :: Maybe Int    ) <- row .:? "storage_id"
    (jobId :: AsvoJobId) <- AsvoJobId <$> row .: "id"

    let obsid = case decimal obs_id of
          Left  _      -> error $ show obs_id <> " could not be converted to Int!"
          Right (i, _) -> if
            | i > (maxBound :: Int)
            -> error $ show obs_id <> " is too big for a machine Int!"
            | i < 1e9 && i > 1e10
            -> error $ show obs_id <> " does not have 10 digits; cannot be an obsid!"
            | otherwise
            -> Obsid i
        jobType = case job_type of
          0 -> Conversion
          1 -> DownloadVisibilities
          2 -> DownloadMetadata
          3 -> DownloadVoltage
          4 -> CancelJob
          _ -> error $ show job_type <> " is an unrecognised job_type!"
        jobState = case job_state of
          0 -> Queued
          1 -> Processing
          2 -> Ready
          3 -> case errorText of
            Nothing -> error "job_state indicates error, but no error_text was given!"
            Just t  -> Error t
          4 -> Expired
          5 -> Cancelled
          _ -> error $ show job_state <> " is an unrecognised job_state!"
    return AsvoJob { .. }

instance A.ToJSON AsvoJob

data AsvoSubmitJobResponseBody = AsvoSubmitJobResponseBody
  { job_id :: {-#UNPACK #-} !Int
  , new :: !Bool
  }

instance A.FromJSON AsvoSubmitJobResponseBody where
  parseJSON = A.withObject "AsvoSubmitJobResponseBody"
    $ \o -> AsvoSubmitJobResponseBody <$> o .: "job_id" <*> o .: "new"
