-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module GiantSquid.ASVO
  ( module GiantSquid.ASVO
  , module GiantSquid.ASVO.Internal
  )
where

import           Codec.Archive.Zip.Conduit.UnZip          ( unZipStream )
import           Conduit
import           Crypto.Hash
import           Crypto.Hash.Conduit                      ( sinkHash )
import qualified Data.ByteString.Char8         as B8
import qualified Data.HashMap.Lazy             as HM
import           Data.Text.Encoding                       ( encodeUtf8 )
import           Data.Time.Clock
import qualified Data.Vector                   as V
import           System.Environment                       ( lookupEnv )

import           GiantSquid.ASVO.Internal
import           GiantSquid.Imports
import           GiantSquid.Types
import           GiantSquid.Utils


-- | Authenticate with the ASVO service.
connectToAsvo :: MonadIO m => m CookieJar
connectToAsvo = do
  apiKey        <- envVar "MWA_ASVO_API_KEY" Nothing
  -- Interfacing with the ASVO server requires specifying the client version.
  -- As this is not the manta-ray-client, we need to lie here.
  -- Use a user-specified value if available, or the hard-coded one here.
  clientVersion <- envVar "MWA_ASVO_VERSION" (Just "mantaray-clientv1.0")
  -- Connect and return the cookie jar.
  responseCookieJar
    <$> httpNoBody (asvoRequest "POST" "login" & setRequestBasicAuth clientVersion apiKey)
 where
  envVar var mDefault =
    (\case
        Nothing -> fromMaybe (errorWithoutStackTrace $ var <> " is not defined.") mDefault
        Just e  -> B8.pack e
      )
      <$> liftIO (lookupEnv var)

{-| Default parameters: Generate a measurement set with 4s time integration,
40kHz frequency channels, flag 160kHz from the edges of each coarse band, allow
missing gpubox files and flag the centre channel of each coarse band.

Note that if you use @defaultConversionParameters@, you need to /overwrite/ any
settings that you don't want to keep (e.g. set "edgewidth" to 0 to disable it).
-}
defaultConversionParameters :: HashMap Text Text
defaultConversionParameters = HM.fromList
  [ ("download_type" , "conversion")
  , ("conversion"    , "ms")
  , ("timeres"       , "4")
  , ("freqres"       , "40")
  , ("edgewidth"     , "160")
  , ("allowmissing"  , "true")
  , ("flagdcchannels", "true")
  ]

-- | Get the ASVO queue in a 'HashMap'. Useful for matching obsids to their jobs.
getAsvoQueue :: (MonadIO m, MonadThrow m) => CookieJar -> m AsvoQueue
getAsvoQueue cookieJar = do
  vec <- getVectorAsvoQueue cookieJar
  {- Check that the vector length is the same as the hashmap length.
This prevents the hashmap silently consuming duplicate keyed entries.
-}
  let hm = HM.fromList $ V.toList $ intermediate <$> vec
  if V.length vec == HM.size hm
    then return hm
    else throwString
      "There are duplicate observation entries in your ASVO queue! This shouldn't happen."
  where intermediate asvoJob@AsvoJob { jobId } = (jobId, asvoJob)

{-| Submit a cotter conversion job, given a 'CookieJar', conversion parameters and
  an 'Obsid'.

Use @encodeConversionParameters@ to convert @'HashMap' 'Text' 'Text'@ to
@'HashMap' 'ByteString' 'ByteString'@
-}
submitAsvoConversionJob
  :: (MonadIO m, MonadThrow m)
  => CookieJar
  -> HashMap ByteString ByteString -- ^ conversion parameters
  -> Obsid
  -> m AsvoJobId
submitAsvoConversionJob cookieJar = submitAsvoJob cookieJar Conversion

-- | Using 'encodeUtf8', convert a 'HashMap' of 'Text' key-value pairs to
-- 'ByteString' key-value pairs.
encodeConversionParameters :: HashMap Text Text -> HashMap ByteString ByteString
encodeConversionParameters hm =
  HM.fromList ((\(k, v) -> (encodeUtf8 k, encodeUtf8 v)) <$> HM.toList hm)

-- | Submit a visibility job, given a 'CookieJar'.
submitAsvoVisibilityJob :: (MonadIO m, MonadThrow m) => CookieJar -> Obsid -> m AsvoJobId
submitAsvoVisibilityJob cookieJar =
  submitAsvoJob cookieJar DownloadVisibilities (HM.singleton "download_type" "vis")

-- | From an 'Obsid' or 'AsvoJobID', return a job's metadata.
getJobMetadataFromParam :: MonadCatch m => AsvoQueue -> Either Obsid AsvoJobId -> m AsvoJob
getJobMetadataFromParam asvoQueue param = case param of
  Right jobId -> case HM.lookup jobId asvoQueue of
    Nothing   -> throw $ JobIdNotInQueue jobId
    Just meta -> testJobMetadata meta
  Left specifiedObsid ->
    case HM.toList $ HM.filter (\AsvoJob { obsid } -> specifiedObsid == obsid) asvoQueue of
      []          -> throw $ ObsidNotInQueue specifiedObsid
      [(_, meta)] -> testJobMetadata meta
      _           -> throw $ MultipleFilesObsidException specifiedObsid
 where
  testJobMetadata meta@AsvoJob {..} = if
    | jobState /= Ready            -> throw $ JobIdNotReady jobId
    | isNothing files              -> throw $ EmptyProductArrayException obsid jobId
    | length (fromJust files) /= 1 -> throw $ NonUnityProductArrayException obsid jobId
    | otherwise                    -> return meta

-- | Download the specified 'Obsid' or 'AsvoJobID'.
downloadAsvoJob
  :: (MonadCatch m, MonadUnliftIO m, PrimMonad m, MonadCatch n)
  => Bool -- ^ Print download information?
  -> CookieJar
  -> Bool -- ^ Hash the download, to check the download integrity?
  -> Bool -- ^ Just download the zip? (i.e. do not stream unzip)
  -> Either Obsid AsvoJobId
  -> m (n ())
downloadAsvoJob verbose cookies hashDownload keepZip param = do
  {- As downloads from the ASVO are typically large (>40 GiB), and we'd
(probably) prefer to halt program execution on download failure, it seems
acceptable to request and parse the user's ASVO queue every time this function
is called.
-}
  asvoQueue <- getAsvoQueue cookies
  case getJobMetadataFromParam asvoQueue param of
    Left  exc  -> throw <$> return exc
    Right meta -> return <$> downloadAsvoJob' verbose cookies hashDownload keepZip meta

{-| Effectively the same as 'downloadAsvoJob', except the 'AsvoJob' type is required,
which allows this function to download without querying the ASVO job queue on every call.
-}
downloadAsvoJob'
  :: (MonadCatch m, MonadUnliftIO m, PrimMonad m)
  => Bool -- ^ Print download information?
  -> CookieJar
  -> Bool -- ^ Hash the download, to check the download integrity?
  -> Bool -- ^ Just download the zip? (i.e. do not stream unzip)
  -> AsvoJob
  -> m ()
downloadAsvoJob' verbose cookies hashDownload keepZip AsvoJob {..} = do
  let obsid'      = unObsid obsid
      jobID'      = unAsvoJobId jobId
      productFile = case files of
        Just (file : _) -> file
        _ ->
          error
            $  "For obsid "
            <> show obsid
            <> " (job ID "
            <> show jobId
            <> "), downloadAsvoJob' was given an empty product list! Cannot continue."
      filename     = fileName productFile
      expectedHash = fileHash productFile
      filesize     = fileSize productFile
      request      = (asvoRequest "GET" "download") { cookieJar = Just cookies } & setQueryString
        [("file_name", Just $ B8.pack filename), ("job_id", Just $ B8.pack $ show jobId)]
      downloadMethod
        :: (MonadIO m, MonadThrow m, PrimMonad m) => ConduitT ByteString Void (ResourceT m) ()
      downloadMethod = if keepZip then sinkFile filename else void unZipStream .| extractZip
      f
        :: (MonadIO m, MonadThrow m, PrimMonad m)
        => p
        -> ConduitT ByteString Void (ResourceT m) (Maybe (Digest SHA1))
      f = if hashDownload
        then (\_ -> getZipSink (ZipSink downloadMethod *> ZipSink (Just <$> sinkHash)))
        else (\_ -> downloadMethod >> return Nothing)

  when verbose $ liftIO $ putStrLn $ formatToString
    ("Downloading job ID " % int % " (obsid: " % int % ", " % bytes (fixed 2 % " ") % ")")
    jobID'
    obsid'
    filesize
  startTime <- liftIO getCurrentTime

  runResourceT (httpSink request f) >>= \case
    Nothing      -> return ()
    Just sha1Sum -> when (show sha1Sum /= expectedHash) $ throwString $ formatToString
      ( "Calculated hash of the ASVO zip did not match the upstream hash!\n"
      % "Obsid: "
      % int
      % ", job ID: "
      % int
      % "\nASVO hash: "
      % string
      % ", calculated hash: "
      % string
      )
      obsid'
      jobID'
      expectedHash
      (show sha1Sum)

  finishTime <- liftIO getCurrentTime
  let timeTaken = diffUTCTime finishTime startTime
      rate      = round $ fromIntegral filesize / timeTaken :: Integer
  when verbose $ liftIO $ putStrLn $ formatToString
    ("Download complete (average rate " % bytes (fixed 2 % " ") % "/s)")
    rate
