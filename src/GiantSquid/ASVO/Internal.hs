-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module GiantSquid.ASVO.Internal
  ( asvoRequest
  , getRawAsvoQueue
  , getVectorAsvoQueue
  , getJsonAsvoQueue
  , printAsvoQueue
  , submitAsvoJob
  , textParametersToHashMap
  , prettyPrintConversionParams
  )
where

import qualified Data.Aeson                    as A
import qualified Data.ByteString.Char8         as B8
import qualified Data.ByteString.Lazy          as BL
import qualified Data.HashMap.Lazy             as HM
import qualified Data.Text                     as T
import           Data.Text.Encoding                       ( decodeUtf8 )
import qualified Data.Vector                   as V
import           Formatting
import           Options.Applicative.Help                 ( indent
                                                          , text
                                                          , vsep
                                                          )
import           Options.Applicative.Help.Pretty          ( Doc )
import           System.IO                                ( stdout )
import qualified Text.PrettyPrint.ANSI.Leijen  as PP

import           GiantSquid.Imports
import           GiantSquid.Types


-- | Make an ASVO request, given a request method (e.g. get_jobs) and type
-- (e.g. GET).
asvoRequest :: ByteString -> ByteString -> Request
asvoRequest requestMethod requestType = defaultRequest { host   = "asvo.mwatelescope.org"
                                                       , port   = 8778
                                                       , secure = True
                                                       , path   = "/api/" <> requestType
                                                       , method = requestMethod
                                                       }

-- | Print the raw JSON output from the ASVO get_jobs request.
getRawAsvoQueue :: MonadIO m => CookieJar -> m BL.ByteString
getRawAsvoQueue cookieJar =
  responseBody <$> httpLBS (asvoRequest "GET" "get_jobs") { cookieJar = Just cookieJar }

-- | Decode JSON into a 'Vector' of @'AsvoJob'@. Useful to print out the jobs in
-- the tracked order.
getVectorAsvoQueue :: MonadIO m => CookieJar -> m (Vector AsvoJob)
getVectorAsvoQueue cookieJar = getRawAsvoQueue cookieJar >>= \raw -> case A.eitherDecode raw of
  Left err ->
    liftIO (putStrLn "Error when attempting to JSON decode the ASVO job queue:\n") >> error err
  Right results -> return results

-- | Return ASVO jobs as a JSON object associating ASVO job IDs with
-- jobs. Useful for letting other programming languages parse the ASVO job
-- states.
getJsonAsvoQueue :: MonadIO m => CookieJar -> m BL.ByteString
getJsonAsvoQueue cookieJar = getRawAsvoQueue cookieJar >>= \raw -> case A.eitherDecode raw of
  Left err ->
    liftIO (putStrLn "Error when attempting to JSON decode the ASVO job queue:\n") >> error err
  Right results ->
    return
      $   A.encode
      $   HM.fromList
      $   V.toList
      $   (\j@AsvoJob { jobId } -> (show $ unAsvoJobId jobId, j))
      <$> results

-- | A list of Int type which keeps the maximum values element-by-element when
-- using (<>).
newtype PadSizes = PadSizes {unPadSizes :: [Int]}
instance Semigroup PadSizes where
  PadSizes a <> PadSizes b = PadSizes $ zipWith max a b

convert :: AsvoJob -> ([Doc], PadSizes)
convert AsvoJob {..} =
  let o       = show $ unObsid obsid
      oDoc    = PP.bold $ PP.text o
      col1Pad = length o + 1

      jid     = show $ unAsvoJobId jobId
      jidDoc  = PP.text jid
      col2Pad = length jid + 1

      jt      = show jobType
      jtDoc   = PP.text jt & case jobType of
        Conversion           -> PP.blue
        DownloadVisibilities -> PP.blue
        DownloadMetadata     -> PP.yellow
        DownloadVoltage      -> PP.magenta
        CancelJob            -> PP.red
      col3Pad = length jt + 1

      js      = show jobState
      jsDoc   = PP.text js & case jobState of
        Queued     -> PP.magenta
        Processing -> PP.blue
        Ready      -> PP.green
        Error _    -> PP.red
        Cancelled  -> PP.red
        Expired    -> PP.dullred
      col4Pad = length js + 1

      sizeDoc = PP.text $ case files of
        Just [AsvoFilesArray { fileSize }] -> formatToString (bytes (fixed 2 % " ")) fileSize
        _ -> ""
  in  ([oDoc, jidDoc, jtDoc, jsDoc, sizeDoc], PadSizes [col1Pad, col2Pad, col3Pad, col4Pad, 0])

printAsvoQueue
  :: CookieJar
  -> Bool -- ^ Print with colour?
  -> IO ()
printAsvoQueue cookieJar colour = getVectorAsvoQueue cookieJar >>= \asvoQueue ->
  if V.null asvoQueue
    then putStrLn "You have no jobs."
    else
      let
          -- This awful mess prints out a "table" of ASVO jobs. It has to determine
          -- the maximum length from each column before padding all elements of that
          -- column, then puts everything back together.
          headerLabels = ["Obsid", "Job Id", "Job Type", "Job State", "File Size"] :: [String]
          (converted, rowPads) = V.unzip $ convert <$> asvoQueue
          headerPads           = PadSizes $ (+ 1) . length <$> headerLabels
          biggestPads          = unPadSizes $ headerPads <> V.foldl1' (<>) rowPads

          f :: [Int] -> [Doc] -> Doc
          f pads docs = foldr (\(p, d) a -> PP.fill p d PP.<+> a) (PP.text "") $ zip pads docs

          doc =
            foldr (\(pads, docs) a -> f pads docs PP.<$> a) (PP.text "")
              $ zip (repeat biggestPads)
              $ (PP.bold . PP.text <$> headerLabels)
              : V.toList converted
      in  if colour then PP.putDoc doc else PP.displayIO stdout $ PP.renderCompact doc

-- | This low-level function actually communicates a job to the ASVO and returns an AsvoJobId.
submitAsvoJob
  :: (MonadIO m, MonadThrow m)
  => CookieJar
  -> AsvoJobType
  -> AsvoJobParameters
  -> Obsid
  -> m AsvoJobId
submitAsvoJob cookieJar jobType parameters obsid = do
  let apiPath = case jobType of
        Conversion           -> "conversion_job" :: ByteString
        DownloadVisibilities -> "download_vis_job"
        _                    -> error "Only conversion and visibility jobs are currently supported."
      jobParameters = HM.toList $ HM.insert "obs_id" (B8.pack $ show obsid) parameters
      -- I don't know why, but setQueryString doesn't work here; urlEncodedBody does.
      request =
        (asvoRequest "POST" apiPath) { cookieJar = Just cookieJar } & urlEncodedBody jobParameters
  response <- httpLBS request
  -- Throw if we didn't get a 200 back.
  case getResponseStatusCode response of
    200 -> case A.eitherDecode $ responseBody response of
      Left e ->
        liftIO
            (putStrLn
              "Error when attempting to JSON decode the response body returned after submitting a job:\n"
            )
          >> error e
      Right AsvoSubmitJobResponseBody {..} -> return $ AsvoJobId job_id
    _ -> throw $ AsvoResponseException jobParameters request $ BL.toStrict <$> response

{-| Convert a comma-separated key-value 'Text' to a 'HashMap' of 'Text' 'Text'

@
textParametersToHashMap "timeres=0.5,freqres=10" == HM.fromList [("timeres","0.5"),("freqres","10")]
@
-}
textParametersToHashMap :: Text -> HashMap Text Text
textParametersToHashMap t =
  HM.fromList $ (\(k, v) -> (k, T.tail v)) . T.breakOn "=" <$> T.splitOn "," (T.filter (/= ' ') t)

prettyPrintConversionParams :: HashMap ByteString ByteString -> Doc
prettyPrintConversionParams cp = indent
  4
  (vsep
    (Options.Applicative.Help.text . T.unpack <$> T.lines
      (T.intercalate "\n" $ (\(k, v) -> decodeUtf8 k <> "=" <> decodeUtf8 v) <$> HM.toList cp)
    )
  )
